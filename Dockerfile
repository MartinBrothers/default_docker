# Copyright (C) 2019 Martin Broers <broers.martin@gmail.com>

# For Alpine, latest is actually the latest stable
FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="Martin Broers <broers.martin@gmail.com>"

# We want the latest stable version from the repo
# hadolint ignore=DL3018
RUN \
    sed -i 's|\(^http.*\)community|&\n\1testing|' "/etc/apk/repositories" && \
    apk add --no-cache \
        dumb-init \
    && \
    rm -rf "/var/cache/apk/"*

COPY "dockerfiles/docker-entrypoint.sh" "/init"

ENTRYPOINT [ "/init" ]
